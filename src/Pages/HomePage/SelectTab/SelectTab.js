import React, { useEffect, useState } from 'react'
import { Button, Select, Space, message } from 'antd';
import { movieServ } from '../../../service/movieService';
import "./select.css";

const items = [
  {
    key: 'tenPhim',
    label: 'tenPhim',
    children: 'phim',
  },
  {
    key: 'tenPhim',
    label: 'tenPhim',
    children: 'phim',
  },

];

export default function SelectTab() {
  const [phim, setPhim] = useState([]); 
  useEffect(()=>{
    movieServ.getMovieByTheater()
    .then((res) => {
      setPhim(res.data.content);
      console.log(res.data.content);
    }).catch((err) => {
      console.log(err);    
    });
  },[])

  const [rap, setRap] = useState([]); 
  useEffect(()=>{
    movieServ.getMovieList()
    .then((res) => {
      setRap(res.data.content);
      console.log(res.data.content);
    }).catch((err) => {
      console.log(err);    
    });
  },[])

  const handleChange = (value) => {
  console.log(`selected ${value}`);

  }

  let renderPhim =() =>{
    return phim.map((tenPhim)=>{
      return{
        key: tenPhim.maHeThongRap,
        value: `${tenPhim.tenHeThongRap}`,
      }
       
      
    })
    }

  const thongbao =()=>{
    message.warning('Chức năng đang cập nhật :<<')
  }
    
  let renderXemPhim =() =>{
    return rap.map((tenPhim)=>{
      return{
        key: tenPhim.tenPhim,
        value: `${tenPhim.tenPhim}`,
      }
       
      
    })
    }







  

  return (
    <div className='container flex justify-center position-relative'>
      <Space wrap class="flex shadow p-4 mb-5 bg-white rounded position-absolute classnew">
    <Select
      defaultValue='Rạp'
      style={{
        width: 200,
      }}
      onChange={handleChange}
      options={renderPhim()}
    />
    <Select
      defaultValue='Phim'
      style={{
        width: 200,
      }}
      onChange={handleChange}
      options={renderXemPhim()}
    />
    <Select
      defaultValue='Cụm Rạp'
      style={{
        width: 200,
      }}
      onChange={handleChange}
      options={renderPhim()}
    />
    <Button
    style={{
      width: 200,
    }}
    onClick={thongbao}
    type="primary"  danger >Đặt Vé</Button>
  </Space>

    </div>
  )
    }
