import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
import './item.css'
import { Button, Modal } from 'antd';
import { useState } from 'react';


const { Meta } = Card;
// const ItemMovie = ({data}) => (
  
//   <Card
//     hoverable
//     cover={<img
//         className='h-60 object-cover'
//         alt="example" src={data.hinhAnh} />}
//   >
//     <Meta
//     className='h-20'
//     // title={data.tenPhim} description={<NavLink className={"px-5 py-2 border-2 border-red-500 rounded"} to={`/detail/${data.maPhim}`} >Xem Ngay</NavLink>} />
//     title={data.tenPhim} description={<div>{data.moTa}</div>} />
//     <div className='item_an'>
//         <div className='item_an1'>
//           <i class="fa fa-play-circle"></i>
//         </div>
//         <div className='item_an2'>
//           <NavLink className={" btn-xem border-2 border-red-500 rounded"} to={`/detail/${data.maPhim}`} >Xem Ngay</NavLink>
//         </div>

//     </div>
//   </Card>

// );
// export default ItemMovie;






const ItemMovie2 = ({data}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
       <Card
    hoverable
    cover={<img
        className='h-60 object-cover'
        alt="example" src={data.hinhAnh} />}
  >
    <Meta
    className='h-20'
    // title={data.tenPhim} description={<NavLink className={"px-5 py-2 border-2 border-red-500 rounded"} to={`/detail/${data.maPhim}`} >Xem Ngay</NavLink>} />
    title={data.tenPhim} description={<div>{data.moTa}</div>} />
    <div className='item_an'>
        <div className='item_an1'>
        <button type="primary" onClick={showModal}>
        <i class="helooo fa fa-play-circle"></i>
      </button>
      <Modal className='w-75' title="" open={isModalOpen} onCancel={handleCancel}>
      <iframe  width="750" height="400"
src={data.trailer}>
</iframe>
      </Modal>
          
        </div>
        <div className='item_an2'>
          <NavLink className={" btn-xem border-2 border-red-500 rounded"} to={`/detail/${data.maPhim}`} >Xem Ngay</NavLink>
        </div>

    </div>
  </Card>
    </>
  );
};
export default ItemMovie2;



