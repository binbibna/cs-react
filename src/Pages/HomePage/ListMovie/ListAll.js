import React from 'react'
import { Desktop, Mobile, Tablet } from "./Responsive2"
import ListMovie from './ListMovie'
import ListMovie2 from './ListMovie2'
import ListMovie3 from './ListMovie3'


export default function ListAll() {
  return (
    <div>
        <Desktop>
            <ListMovie />
        </Desktop>
        <Tablet >
            <ListMovie2 />
        </Tablet>
        <Mobile>
            <ListMovie3 />
        </Mobile>
    </div>
  )
}
