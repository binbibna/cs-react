import React, { useEffect, useState } from 'react'
import { movieServ } from '../../../service/movieService'
import ItemMovie2 from './ItemMovie2';
import { Carousel } from 'antd';
import './list.css';

const contentStyle = {

  height: '100%',
  color: '#fff',
  lineHeight: '160px',

  background: 'white',
};

export default function ListMovie3() {

  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
    const [movies, setMovies] = useState([]);
    useEffect(()=> {
        movieServ.getMovieList()
            .then((res) =>{
                console.log(res);
                setMovies(res.data.content);
            })
            .catch((err)=>{
                console.log(err);
            })
    },[])
  return (
    <div id='danhsach' className='container containerv3' >
    {/* <div className='container grid grid-cols-4 gap-10'>
      {movies.slice(9, 1).map((item) =>{
        return <ItemMovie data={item} key={item.maPhim} />
      })}
    </div> */}


    <Carousel afterChange={onChange}>
      <div>
       
        <div style={contentStyle} className='p-3 grid grid-cols-1 gap-5 mobile'>
      {movies.slice(0, 8).map((item) =>{
        return <ItemMovie2 data={item} key={item.maPhim} />
      })}
        </div>
      </div>
      <div>
      <div style={contentStyle} className='p-3 grid grid-cols-1 gap-5 mobile'>
      {movies.slice(9, 17).map((item) =>{
        return <ItemMovie2 data={item} key={item.maPhim} />
      })}
        </div>
      </div>
      <div>
      <div style={contentStyle} className='p-3 grid grid-cols-1 gap-5 mobile'>
      {movies.slice(18, 26).map((item) =>{
        return <ItemMovie2 data={item} key={item.maPhim} />
      })}
        </div>
      </div>
    </Carousel>

    </div>
  )
}
