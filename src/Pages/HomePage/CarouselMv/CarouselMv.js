import React, { useState } from 'react'
import { Button, Modal } from 'antd';
import { NavLink } from 'react-router-dom';
import './crs.css'

export default function CarouselMv() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    
        <div id="carouselExampleControls" className="carousel slide mt-0" data-ride="carousel">
          <div className="carousel-inner">
            <div className="carousel-item active">
                <img className="d-block w-100" src="https://s3img.vcdn.vn/123phim/2021/04/ban-tay-diet-quy-evil-expeller-16177781815781.png" alt="First slide" />
                <div className='item_an'>
                    <div className='item_an1'>
                    <button type="primary" onClick={showModal}>
                    <i class="helooo fa fa-play-circle"></i>
                  </button>
                  <Modal className='w-75' title="" open={isModalOpen} onCancel={handleCancel}>
                  <iframe className='w-100 h-100' src="https://www.youtube.com/embed/uqJ9u7GSaYM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                  </Modal>
                      <h1>Demo thui hehe</h1>
                    </div>

                </div>
            </div>
            <div className="carousel-item">
                <img className="d-block w-100" src="https://s3img.vcdn.vn/123phim/2021/04/nguoi-nhan-ban-seobok-16177781610725.png" alt="Second slide" />
                <div className='item_an'>
                    <div className='item_an1'>
                    <button type="primary" onClick={showModal}>
                    <i class="helooo fa fa-play-circle"></i>
                  </button>
                  <Modal className='w-75' title="" open={isModalOpen} onCancel={handleCancel}>
                 <iframe className='w-100 h-100' src="https://www.youtube.com/embed/JNZv1SgHv68" title="YouTube video" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" ></iframe>
                  </Modal>
                  <h1>Demo thui hehe</h1>
                    </div>

                </div>
            </div>
            <div className="carousel-item">
                <img className="d-block w-100" src="https://s3img.vcdn.vn/123phim/2021/04/lat-mat-48h-16177782153424.png" alt="Third slide" />
                <div className='item_an'>
                    <div className='item_an1'>
                    <button type="primary" onClick={showModal}>
                    <i class="helooo fa fa-play-circle"></i>
                  </button>
                  <Modal className='w-75' title="" open={isModalOpen} onCancel={handleCancel}>
                  <iframe className='w-100 h-100' src="https://www.youtube.com/embed/uqJ9u7GSaYM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                  </Modal>
                      <h1>Demo thui hehe</h1>
                    </div>

                </div>
            </div>
        </div>
        <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true" />
        <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true" />
        <span className="sr-only">Next</span>
        </a>
        </div>
    
  )
}
