import React from 'react'
import Header from '../../Components/Header/Header'
import ListMovie from './ListMovie/ListMovie'
import TabsMovie from './TabsMovie/TabsMovie'
import CarouselMv from './CarouselMv/CarouselMv'
import Footer from '../../Components/Footer/Footer'
import TabMv from './tabs/TagMv'
import SelectTab from './SelectTab/SelectTab'
import ListAll from './ListMovie/ListAll'


export default function HomePage() {
  return (
    <div className='space-y-10'>
      <Header />
      <CarouselMv />
      <SelectTab />
      <ListAll />
      <TabsMovie />
      <TabMv />
      <Footer/>
    </div>
  )
}
