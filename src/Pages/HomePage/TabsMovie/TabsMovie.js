import React, { useEffect, useState } from 'react'
import { movieServ } from '../../../service/movieService'
import { Tabs } from 'antd';
import ItemMovie from '../ListMovie/ItemMovie';
import ItemTabMovies from './ItemTabMovies';


const onChange = (key) => {
  console.log(key);
};
const items = [
  {
    key: '1',
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
];

export default function TabsMovie() {
    const [heThongRap, setHeThongRap] = useState([]);
    useEffect(() => {
        movieServ.getMovieByTheater()
            .then((res)=> {
                setHeThongRap(res.data.content);
                console.log(res);
            })
            .catch((err)=>{
                console.log(err);
            })
    }, [])
    let renderHeThongRap =() => {
        return heThongRap.map((rap) => {
            return {
                key: rap.maHeThongRap,
                label: <img 
                className='h-16'
                src={rap.logo} alt="" />,
                children: (
                    <Tabs
                        tabPosition='left'
                        defaultActiveKey="1" 
                        items={rap.lstCumRap.map((cumRap) =>{
                            return{
                                key: cumRap.maCumRap,
                                label: <div>{cumRap.tenCumRap}</div>,
                                children: (<div style={{height:500}} className=' overflow-y-scroll'> {cumRap.danhSachPhim.map((item)=> {
                                    return <ItemTabMovies phim={item} />
                                })}
                                </div>
                                ),
                            }
                        })} 
                        onChange={onChange} />
                ),
              };
        })
    }
  return (
    <div id='tab' className='container'>
      <Tabs
      tabPosition='left'
      defaultActiveKey="1" 
      items={renderHeThongRap()}
      onChange={onChange} />
    </div>
  )
}
