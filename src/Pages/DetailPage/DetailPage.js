import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieServ } from '../../service/movieService';
import { Progress } from 'antd';

export default function DetailPage() {
    let {id} = useParams();
    const [movie, setMovie] = useState({});
    console.log(id);
    useEffect(()=>{
        
        let fetchDetail = async()=>{
            try {
                let result = await movieServ.getDetailMovie(id);
                setMovie(result.data.content);
                console.log(result);
            } catch (err) {
                console.log(err);
            }
            
            
        };
        fetchDetail();

    },[])
  return (
    <div className='container'>
        <div className="flex space-x-10">
            <div className='w-25'>
                <img className='w-100' src={movie.hinhAnh} alt="" />
                <div className='my-4 flex justify-between '>
                    <NavLink
                    className="rounded p-4 bg-red-600 text-white"
                    to={`/booking/${id}`}>
                        Mua Vé
                    </NavLink>
                    <NavLink to='/'
                    className="rounded p-4 bg-red-600 ml-4 text-white"
                    >
                    
                        Quay Về

                    </NavLink>
                </div>
            </div>
            <div className='w-75'>
                <h2 className='font-medium'>{movie.tenPhim}</h2>
                <h2>{movie.moTa}</h2>
                <Progress percent={movie.danhGia*10}/>
            </div>
        </div>
        
        
    </div>
  )
}
