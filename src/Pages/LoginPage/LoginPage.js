import React from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { userServ } from '../../service/userService';
import { localUserServ } from '../../service/localService';
import { NavLink, useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import bg_animate from "../../asset/login_animate.json";
import { setLoginAction, setLoginActionService } from '../../redux/action/userAction';
import { useDispatch } from 'react-redux';
import { setUserLogin } from '../../toolkit/userSlice';
import "./login.css"


const LoginPage = () => {
    let dispatch = useDispatch();
    let navigate = useNavigate();
    //redux thường
    const onFinish = (values: any) => {
        console.log('Success:', values);
        userServ.postLogin(values)
              .then((res) => {
                  message.success("login thành công");
                  // lưu thông tin user vào localStorage
                  localUserServ.set(res.data.content);
                  dispatch(setLoginAction(
                    res.data.content
                  ));
                  // chuyển hướng user tới trang chủ
                  navigate("/");
                  console.log(res);
              })
              .catch((err) => {
                  message.error("đăng nhập thất bại");
                  console.log(err);
              })
      };
    // end redux thường
    

    // redux toolkit
    const onFinishToolkit = (values: any) => {
        console.log('Success:', values);
        userServ.postLogin(values)
              .then((res) => {
                  message.success("login thành công");
                  // lưu thông tin user vào localStorage
                  localUserServ.set(res.data.content);
                  // dispatch(setLoginAction(
                  //   res.data.content
                  // ));
                  dispatch(setUserLogin(res.data.content));
                  // chuyển hướng user tới trang chủ
                  navigate("/");
                  console.log(res);
              })
              .catch((err) => {
                  message.error("Tài Khoản hoặc mật khẩu không đúng");
                  console.log(err);
              })
      };
    //end
    
    // redux thunk
    const onFinishThunk = (values) =>{
      let onSuccess = () =>{
        message.success("Login thành công");
        navigate("/");
      }
        // callback
        dispatch(setLoginActionService(values,onSuccess))
      }
    //end


    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
        
      };
    return (
  <div class="h-screen w-screen flex bg-orange-500 justify-center items-center pageuser">
      <div class="container w-25 mx-auto p-5 bg-white rounded flex flex-col lg:flex-row">
        {/* <div className="w-full lg:w-1/2 h-full">
        <Lottie className='w-1/2 desktop:w-full' animationData={bg_animate} loop={true} />
        </div> */}
        <div className='flex justify-between'>
          <h1 className='mb-2 '>Đăng Nhập</h1>
          <NavLink to='/signup'><button>Đăng Kí</button></NavLink>
        </div>
        <div className="w-full lg:w-1/2 h-full">
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 24 }}
            style={{ width: "100%" }}
            initialValues={{ remember: true }}
            onFinish={onFinishToolkit}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item className="flex justify-center" wrapperCol={{span: 24 }}>
              <Button className="bg-orange-500 hover:text-white hover:border-hidden" htmlType="submit">
                Đăng Nhập
              </Button>
            </Form.Item>
          </Form>
        </div>
          
      </div>
  </div>
);
}


export default LoginPage;