import React from 'react'
import './footer.css'

export default function Footer() {
  return (
    <div id='footer' className='h-20 border border-black footer'>

          <div className="footer_top">
            <div className="container flex">
              <div className="top_left">
                  <h1 className='ten'>Ứng dụng tiện lợi dành cho người yêu điện ảnh</h1>
                  <h1 className='detail'>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</h1>
                  <button className='btn btn01'><a target='_blank' href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197">APP MIỄN PHÍ - TẢI VỀ NGAY!</a></button>
              </div>
              <img className='logone' src="https://scontent.fsgn2-5.fna.fbcdn.net/v/t1.15752-9/340808710_1189445108383288_6884642062645223245_n.png?_nc_cat=104&ccb=1-7&_nc_sid=ae9488&_nc_ohc=NThVUQEEedsAX-sG4-S&_nc_ht=scontent.fsgn2-5.fna&oh=03_AdSx_fqaL5mW2bAWMsahiAsZt2BXmS9ZqhVOw-9vxhS9rA&oe=64636905" alt="logo" />
            </div>
          </div>
          <div className="footer_bottom">
            <div className="containerv2">
              <div className="bot_top flex">
                  <div className="itemtop1">
                    <h1>TIX</h1>
                    <div className="row">
                      <div className="col-6">FAQ</div>
                      <div className="col-6">Thỏa thuận sử dụng</div>
                      <div className="col-6">Brand Guidelines</div>
                      <div className="col-6">Chính sách bảo mật</div>
                    </div>
                  </div>
                  <div className="itemtop3">
                    <h1>MOBILE APP</h1>


                  </div>
                  <div className="itemtop4">
                    <h1>SOCIAL</h1>

                    
                  </div>
              </div>
              <hr />
              <div className="bot_bot flex">
                <div className="logobot">dsdsds</div>
                <div className="detailbot">
                  <h1>TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</h1>
                  <span>Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam. <br />
                        Giấy chứng nhận đăng ký kinh doanh số: 0101659783, <br />
                        đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
                        Số Điện Thoại (Hotline): 1900 545 436
                  </span>
                </div>
                <div className="chungchi">dss</div>
              </div>
            </div>
            
            
          </div>
        </div>

  )
}
