import React from 'react'
import { NavLink } from 'react-router-dom'
import UserMenu from './UserMenu'
import './menu.css'


export default function HeaderTablet() {
  return (

    <nav className=" navbar navbar-expand-lg navbar-light bg-light">
  <NavLink to="/">
            <span className="font-medium text-2xl animate-pulse">CyberFlix</span>
            </NavLink>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon" />
  </button>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <div className='flex flex-column'>
            <a href="#danhsach">Danh Sách Phim</a>
            <a  href="#tab">Rạp</a>
            <a href="#footer">APP</a>
            </div>
    <UserMenu />
  </div>
</nav>



  )
}
