import React from 'react'
import UserMenu from './UserMenu'
import { NavLink } from 'react-router-dom'
import './header.css'

export default function HeaderDesktop() {
  return (
    <div className="h-20 shadow w-full">
        <div className="px-5 mx-auto h-full flex items-center justify-between">

            <NavLink to="/">
            <span className="font-medium text-2xl animate-pulse">CyberFlix</span>
            </NavLink>


            <div>
            <a href="#danhsach">Danh Sách Phim</a>
            <a className='mx-5' href="#tab">Rạp</a>
            <a href="#footer">APP</a>
            </div>



            <UserMenu />
        </div>
      
    </div>
  )
}
