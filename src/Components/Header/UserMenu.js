import React from 'react'
import { useSelector } from 'react-redux'
import { localUserServ } from '../../service/localService';
import { NavLink } from 'react-router-dom';
import UserDropDown from './UserDropDown';

export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  })
  console.log(userInfo);
  let handleLogout = () => {
    localUserServ.remove();
    window.location.reload();
    // window.location.href="/login";
  }
  let renderContent = () =>{
    let buttonCss = "px-5 py=2 border-2 border-black rounded";
    if(userInfo){
      return(
        <>
        
        <UserDropDown 
        user={userInfo}
        logoutBtn={
          <button
        onClick={handleLogout}
        className={buttonCss}>Đăng xuất</button>
        }
        
        />
        
        {/* <button
        onClick={handleLogout}
        className={buttonCss}>Đăng xuất</button> */}
      </>
      )
    }else{
      return(
        <>
        <NavLink to="/login">
          <button className={buttonCss}>Đăng Nhập</button>
        </NavLink>
        
        <NavLink to="/signup">
          <button className={buttonCss}>Đăng Kí</button>
        </NavLink>
      </>
      )
    }
  }
  return (
    <div className='space-x-5 flex items-center'>
      {renderContent()}
    </div>
  )
}
